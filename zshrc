# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH=/home/zug/.oh-my-zsh

ZSH_DISABLE_COMPFIX="true"

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
POWERLEVEL9K_MODE='awesome-fontconfig'
POWERLEVEL9K_PROMPT_ON_NEWLINE=true
POWERLEVEL9K_PROMPT_ADD_NEWLINE=true

POWERLEVEL9K_MULTILINE_FIRST_PROMPT_PREFIX=""
POWERLEVEL9K_MULTILINE_LAST_PROMPT_PREFIX=" %F{green}\Uf054%F{reset} "

POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(os_icon context dir custom_kube dropbox go_version vcs)
POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(status dir_writable background_jobs command_execution_time time history)

## for code ({000..255}) print -P -- "$code: %F{$code}This is how your text would look like%f"
## getColorCode background
## getColorCode foreground

POWERLEVEL9K_OS_ICON_BACKGROUND='019'
POWERLEVEL9K_OS_ICON_FOREGROUND='231'

POWERLEVEL9K_GO_VERSION_BACKGROUND='167'
POWERLEVEL9K_GO_VERSION_FOREGROUND='231'

POWERLEVEL9K_CONTEXT_DEFAULT_BACKGROUND='027'
POWERLEVEL9K_CONTEXT_DEFAULT_FOREGROUND='231'
POWERLEVEL9K_CONTEXT_SUDO_BACKGROUND='027'
POWERLEVEL9K_CONTEXT_SUDO_FOREGROUND='231'
POWERLEVEL9K_CONTEXT_REMOTE_BACKGROUND='029'
POWERLEVEL9K_CONTEXT_REMOTE_FOREGROUND='231'
POWERLEVEL9K_CONTEXT_REMOTE_SUDO_BACKGROUND='029'
POWERLEVEL9K_CONTEXT_REMOTE_SUDO_FOREGROUND='231'
POWERLEVEL9K_CONTEXT_ROOT_BACKGROUND='088'
POWERLEVEL9K_CONTEXT_ROOT_FOREGROUND='231'

POWERLEVEL9K_DIR_HOME_BACKGROUND='004'
POWERLEVEL9K_DIR_HOME_FOREGROUND='231'
POWERLEVEL9K_DIR_HOME_SUBFOLDER_BACKGROUND='004'
POWERLEVEL9K_DIR_HOME_SUBFOLDER_FOREGROUND='231'
POWERLEVEL9K_DIR_ETC_BACKGROUND='004'
POWERLEVEL9K_DIR_ETC_FOREGROUND='231'
POWERLEVEL9K_DIR_DEFAULT_BACKGROUND='004'
POWERLEVEL9K_DIR_DEFAULT_FOREGROUND='231'

POWERLEVEL9K_CUSTOM_KUBE="kube"
POWERLEVEL9K_CUSTOM_KUBE_BACKGROUND='067'
POWERLEVEL9K_CUSTOM_KUBE_FOREGROUND='231'

POWERLEVEL9K_DIR_WRITABLE_FORBIDDEN_BACKGROUND='166'
POWERLEVEL9K_DIR_WRITABLE_FORBIDDEN_FOREGROUND='231'

POWERLEVEL9K_STATUS_OK_BACKGROUND='064'
POWERLEVEL9K_STATUS_OK_FOREGROUND='231'
POWERLEVEL9K_STATUS_ERROR_BACKGROUND='009'
POWERLEVEL9K_STATUS_ERROR_FOREGROUND='231'

POWERLEVEL9K_BACKGROUND_JOBS_BACKGROUND='178'
POWERLEVEL9K_BACKGROUND_JOBS_FOREGROUND='231'

POWERLEVEL9K_COMMAND_EXECUTION_TIME_BACKGROUND='240'
POWERLEVEL9K_COMMAND_EXECUTION_TIME_FOREGROUND='231'

POWERLEVEL9K_TIME_BACKGROUND='238'
POWERLEVEL9K_TIME_FOREGROUND='231'

POWERLEVEL9K_HISTORY_BACKGROUND='236'
POWERLEVEL9K_HISTORY_FOREGROUND='231'

POWERLEVEL9K_COMMAND_EXECUTION_TIME_THRESHOLD=0

POWERLEVEL9K_SHORTEN_DIR_LENGTH=5
POWERLEVEL9K_SHORTEN_DELIMITER="…"
# POWERLEVEL9K_SHORTEN_STRATEGY="truncate_to_unique"
POWERLEVEL9K_SHORTEN_STRATEGY="Default"

# VCS Config
POWERLEVEL9K_SHOW_CHANGESET=true

POWERLEVEL9K_VCS_GIT_ICON=''
POWERLEVEL9K_VCS_CLEAN_BACKGROUND='064'
POWERLEVEL9K_VCS_CLEAN_FOREGROUND='231'
POWERLEVEL9K_VCS_UNTRACKED_BACKGROUND='178'
POWERLEVEL9K_VCS_UNTRACKED_FOREGROUND='231'
POWERLEVEL9K_VCS_MODIFIED_BACKGROUND='166'
POWERLEVEL9K_VCS_MODIFIED_FOREGROUND='231'

POWERLEVEL9K_GO_ICON=''

POWERLEVEL9K_LINUX_ICON=''
POWERLEVEL9K_LINUX_ARCH_ICON=''
POWERLEVEL9K_LINUX_DEBIAN_ICON=''
# POWERLEVEL9K_LINUX_ARCH_ICON=''
# POWERLEVEL9K_HOME_ICON='🏡'
# POWERLEVEL9K_HOME_SUB_ICON='📂'
# POWERLEVEL9K_FOLDER_ICON='📁'
# POWERLEVEL9K_VCS_GIT_ICON=''

ZSH_THEME="powerlevel9k/powerlevel9k"

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
HIST_STAMPS="yyyy-mm-dd"

# Would you like to use another custom folder than $ZSH/custom?
ZSH_CUSTOM=$HOME/.workenv/plugins/zsh/custom

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(golang)

if [[ -d ~/.zsh_completions ]]; then
    fpath=(~/.zsh_completions $fpath)
fi

source $ZSH/oh-my-zsh.sh

# Fix broken LC_CTYPE
export LC_CTYPE=${LANG%%:*} # pick the first entry from LANG

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

autoload -U zmv
autoload -U bashcompinit && bashcompinit
autoload -U compinit && compinit -u

if [[ -d ~/.completions ]]; then
    for COMPFILE in ~/.completions/*; do
        source $COMPFILE
    done
fi

eval "$(dircolors "$HOME/.workenv/plugins/zsh/dir_colors")"

if [[ $TERM == xterm-termite || $TERM == xterm-256color ]]; then
  . /etc/profile.d/vte.sh
  type __vte_osc7 &>/dev/null; [[ $? -eq 0 ]] && __vte_osc7
fi

alias fuck='sudo $(fc -ln -1)'
alias bc='bc -ql'
alias less='less -R'
alias vim='nvim'
alias view='nvim -RM'
alias vimdiff='nvim -d'
alias diff='nvim -d'
alias c='clear; printf "\x1b[3J"'
alias sudo='sudo -E'
alias ptree='ps -jefH --forest'
alias highlight='highlight -O truecolor --config-file=/usr/share/highlight/themes/edit-kwrite.theme'
alias unlock='echo | gpg --output /dev/null --sign --quiet'

function serve(){
    echo "http://$(ip address | grep "inet " | grep "scope global dynamic" | sed "s/\s\+inet \([^\/]\+\).*/\1/" | head -n1):${1:-8000}"
    python -m http.server ${1:-8000}
}

function ftar(){
    if [[ -z ${1} ]]; then echo "\x1b[31mTarget to compress must be set\x1b[0m"; return 1; fi
    time tar -c -I zstd -f ${1}.tar.zst ${1}
}

function funtar(){
    if [[ -z ${1} ]]; then echo "\x1b[31mTarget to decompress must be set\x1b[0m"; return 1; fi
    time tar -x -I zstd -f ${1}
}

function fstar(){
    if [[ -z ${1} ]]; then echo "\x1b[31mTarget to display must be set\x1b[0m"; return 1; fi
    time tar -t -I zstd -f ${1}
}

function sshkey(){
    if [[ -z "${1}" ]]; then echo "\x1b[31mComment must be set (user@host)\x1b[0m"; return 1; fi
    if [[ -z "${2}" ]]; then echo "\x1b[31mFilename must be set (project_environment)\x1b[0m"; return 1; fi
    ssh-keygen -q -t rsa -b 4096 -C "${1}" -f "${2}.key" -N ""
    cat "${2}.key.pub"
    cat "${2}.key.pub" | xsel --clipboard
}

function pwgen1(){
    lenght=20
    if [[ -n "$1" ]]; then lenght=$1; fi
    pwd=$(pwgen -1 $lenght); b64=$(printf $pwd | base64); apr1=$(openssl passwd -apr1 -salt $(pwgen 8 -1) $pwd)
    echo -e "Passwd: $pwd\nBase64: $b64\nApr1  : $apr1"
    echo -n "$pwd" | xsel --clipboard
}

## GPG-Agent (as SSH-AGENT)

# Start the gpg-agent if not already running
if ! pgrep -x -u "${USER}" gpg-agent >/dev/null 2>&1; then
  gpg-connect-agent /bye >/dev/null 2>&1
fi

# Set SSH to use gpg-agent
unset SSH_AGENT_PID
if [ "${gnupg_SSH_AUTH_SOCK_by:-0}" -ne $$ ]; then
  export SSH_AUTH_SOCK="/run/user/$UID/gnupg/S.gpg-agent.ssh"
fi

# Customize to your needs...
if [[ -s "${ZDOTDIR:-$HOME}/.zshrc.local" ]]; then
    source "${ZDOTDIR:-$HOME}/.zshrc.local"
fi

export SAVEHIST=999
