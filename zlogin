
# Start X on startup
# [[ -z $DISPLAY && $XDG_VTNR -eq 1 ]] && exec startx -- -ardelay 200 -arinterval 13 -keeptty -nolisten tcp &> /dev/null || true

# Customize to your needs...
if [[ -s "${ZDOTDIR:-$HOME}/.zlogin.local" ]]; then
    source "${ZDOTDIR:-$HOME}/.zlogin.local"
fi
